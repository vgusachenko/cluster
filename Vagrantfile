# -*- mode: ruby -*-
# vi: set ft=ruby :

ENV['VAGRANT_NO_PARALLEL'] = 'yes'

Vagrant.configure("2") do |config|

  #### DEFAULTS ####
  config.vm.box      =  "valengus/flatcar-k8s"
  config.vm.provider :libvirt do |libvirt|
    libvirt.qemu_use_session           = false
    libvirt.management_network_name    = 'kubernetes'
    libvirt.management_network_keep    = true
    libvirt.management_network_domain  = 'local'
    libvirt.management_network_address = '192.168.77.0/24'
  end

  #### MASTER ####
  config.vm.define "master"  do |config|
    config.vm.hostname =  "master"
    config.vm.provider :libvirt do |libvirt|
      libvirt.cpus                       = 2
      libvirt.memory                     = 4  *  1024
    end
    config.vm.provision "shell", inline: <<-SHELL
    systemctl restart systemd-networkd
    
    kubeadm init
    mkdir  -p  /home/core/.kube
    cp  -i /etc/kubernetes/admin.conf /home/core/.kube/config
    chown -R core:core /home/core/.kube
    export KUBECONFIG=/etc/kubernetes/admin.conf
    kubectl apply -f https://docs.projectcalico.org/manifests/calico.yaml
    kubectl wait --for=condition=Ready pods --all -n kube-system  --timeout=120s ; sleep 10 
    kubectl wait --for=condition=Ready nodes --all --timeout=600s ; kubectl get nodes
    kubectl taint node "master" node-role.kubernetes.io/control-plane:NoSchedule-
    kubectl get configmap kube-proxy -n kube-system -o yaml | sed -e "s/strictARP: false/strictARP: true/" | kubectl apply -f - -n kube-system ; sleep 10

    kubectl create namespace argocd
    kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
    kubectl wait --for=condition=Ready pods --all -n argocd --timeout=120s ; sleep 10
    # kubectl --namespace argocd get secret argocd-initial-admin-secret --output jsonpath="{.data.password}" | base64 -d
    kubectl apply -f /vagrant/argocd/vagrant/seed.yml

    SHELL
  end

  # config.vm.define "worker01"  do |config|
  #   config.vm.hostname =  "worker01"
  #   config.vm.provider :libvirt do |libvirt|
  #     libvirt.cpus                       = 2
  #     libvirt.memory                     = 4  *  1024
  #   end
  #   config.vm.provision "shell", inline: <<-SHELL
  #     systemctl restart systemd-networkd
  #     $(ssh -o 'StrictHostKeyChecking=no' -i /vagrant/.vagrant/machines/master/libvirt/private_key core@master /opt/bin/kubeadm token create --print-join-command)
  #   SHELL
  # end

end
